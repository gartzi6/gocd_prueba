FROM python:2
COPY CPULoadGenerator/* /
RUN apt-get update && apt install -y python && apt install -y  python-pip && pip install matplotlib && pip install psutil && pip install twisted && pip install requests
CMD python CPULoadGenerator.py
